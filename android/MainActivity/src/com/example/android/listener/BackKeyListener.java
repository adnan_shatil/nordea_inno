package com.example.android.listener;

import android.view.KeyEvent;

public interface BackKeyListener {
	public void onKeyDown(int keyCode, KeyEvent event);
}
