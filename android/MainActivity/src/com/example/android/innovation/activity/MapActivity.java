package com.example.android.innovation.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.example.android.innovation.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class MapActivity extends FragmentActivity {

	// Google Map
	private GoogleMap googleMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_fragment);

		try {
			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * function to load map. If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			} else {
				setUpMap();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}

	private void setUpMap() {

		// googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0))
		// .title("Marker"));

		googleMap.addCircle(new CircleOptions()
				.center(new LatLng(60.169076, 24.933238)).radius(1000)
				.strokeWidth(0.1f).fillColor(0x55FF0000));
		googleMap.addCircle(new CircleOptions()
				.center(new LatLng(60.217395, 24.807244)).radius(1000)
				.strokeWidth(0.1f).fillColor(0x55FF0000));

		ArrayList<LatLng> latlangs = new ArrayList<LatLng>();
		latlangs.add(new LatLng(60.169076, 24.933238));
		latlangs.add(new LatLng(60.217395, 24.807244));
		zoomToCoverAllMarkers(latlangs, googleMap);

	}

	private void zoomToCoverAllMarkers(ArrayList<LatLng> latLngList,
			GoogleMap googleMap) {
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		/*
		 * for (Marker marker : markers) {
		 * builder.include(marker.getPosition()); }
		 */
		for (LatLng marker : latLngList) {
			builder.include(marker);
		}
		LatLngBounds bounds = builder.build();
		int padding = 0; // offset from edges of the map in pixels
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
		googleMap.moveCamera(cu);
		googleMap.animateCamera(cu);
	}
}
