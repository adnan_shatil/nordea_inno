package com.example.android.innovation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.innovation.R;

public class ItemListAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final String[] values;

	private Integer[] mThumbIds = { R.drawable.house2,R.drawable.audi_a1, R.drawable.audi_a2,
			R.drawable.audi_a3,R.drawable.camera_2_xxl, R.drawable.audi_a1, R.drawable.audi_a2,
			R.drawable.audi_a3, R.drawable.audi_a2

	};

	public ItemListAdapter(Context context, String[] values) {
		super(context, R.layout.suggested_goal_row, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.suggested_goal_row, parent,
				false);
		TextView nameView = (TextView) rowView.findViewById(R.id.item_name);
		TextView detailView = (TextView) rowView.findViewById(R.id.item_detail);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.item_icon);
		nameView.setText(values[position]);
		 imageView.setImageResource(mThumbIds[position]);
	

		return rowView;
	}

}
