package com.example.android.innovation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.innovation.R;

public class ImageAdapter extends BaseAdapter {

	private Context mContext;
	private String[] mChartTitle;

	public ImageAdapter(Context c) {
		mContext = c;
		mChartTitle = mContext.getResources().getStringArray(
				R.array.chart_title_array);
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gridView;
		if (convertView == null) {
			gridView = new View(mContext);
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.graph_grid_item, null);

			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.chart_icon);
			imageView.setImageResource(mThumbIds[position]);

			TextView nameView = (TextView) gridView
					.findViewById(R.id.chart_name);
			nameView.setText(mChartTitle[position]);

		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	// references to our images
	private Integer[] mThumbIds = { R.drawable.linechart1,
			R.drawable.barchart3_h600, R.drawable.laheatmap,
			R.drawable.bubble_chart

	};
	
}
