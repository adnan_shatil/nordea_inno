package com.example.android.innovation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.innovation.R;

public class SelectedItemAdapter extends BaseAdapter {

	private Context context;
	private Integer[] mThumbIds = { R.drawable.audi_a1,
			R.drawable.audi_a2, R.drawable.audi_a3
	};
	private String[] nameArray,priceArray,typeArray;
	
	public SelectedItemAdapter(Context c) {
		this.context = c;
		nameArray=c.getResources().getStringArray(
				R.array.car_name_array);
		priceArray=c.getResources().getStringArray(
				R.array.car_price_array);
		typeArray=c.getResources().getStringArray(
				R.array.car_type_array);
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gridView;
		if (convertView == null) {
			gridView = new View(context);
			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.select_grid_item, null);

			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.grid_item_image);
			imageView.setImageResource(mThumbIds[position]);
			
			TextView nameView=(TextView) gridView.findViewById(R.id.selected_item_name);
			nameView.setText(nameArray[position]);
			TextView typeView=(TextView) gridView.findViewById(R.id.selected_item_type);
			typeView.setText(typeArray[position]);
			TextView priceView=(TextView) gridView.findViewById(R.id.selected_item_price);
			priceView.setText(priceArray[position]);
		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	// references to our images

}
