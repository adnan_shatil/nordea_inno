package com.example.android.innovation.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.android.innovation.R;
import com.example.android.innovation.adapter.ItemListAdapter;

public class SuggestedGoalFragment extends Fragment {

	ListView itemList;
	ItemListAdapter itemAdapter;
	String[] values;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.suggested_goal_list,
				container, false);
		String title = getResources().getString(R.string.suggested_goal);
		getActivity().setTitle(title);
		values = getResources().getStringArray(R.array.suggested_items);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		itemList = (ListView) view.findViewById(R.id.list);
		itemAdapter = new ItemListAdapter(getActivity(), values);
		itemList.setAdapter(itemAdapter);

	}

}
