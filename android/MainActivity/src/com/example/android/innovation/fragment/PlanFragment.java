package com.example.android.innovation.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.android.innovation.R;
import com.example.android.innovation.adapter.ObjectAdapter;
import com.example.android.innovation.adapter.SelectedItemAdapter;

public class PlanFragment extends android.app.Fragment implements
		OnItemSelectedListener, OnClickListener {

	Button goButton, selectButton,suggestionButton;
	RelativeLayout buyLayout, selectLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.target, container, false);
		String title = getResources().getString(R.string.set_goal);
		getActivity().setTitle(title);

		buyLayout = (RelativeLayout) rootView
				.findViewById(R.id.object_select_layout);
		selectLayout = (RelativeLayout) rootView
				.findViewById(R.id.show_item_layout);

		// First Screen GridView
		GridView gridview = (GridView) rootView
				.findViewById(R.id.object_gridview);
		gridview.setAdapter(new ObjectAdapter(getActivity()));

		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

			}
		});

		// Second Screen Gridview

		GridView itemDetailGridView = (GridView) rootView
				.findViewById(R.id.found_item_list);
		itemDetailGridView.setAdapter(new SelectedItemAdapter(getActivity()));

		itemDetailGridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

			}
		});

		Spinner spinner_one = (Spinner) rootView
				.findViewById(R.id.select_model_spinner);
		Spinner spinner_two = (Spinner) rootView
				.findViewById(R.id.year_spinner);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<CharSequence> adapter_one = ArrayAdapter
				.createFromResource(getActivity(), R.array.car_model_array,
						android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_one
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner_one.setAdapter(adapter_one);
		spinner_one.setOnItemSelectedListener(this);

		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<CharSequence> adapter_two = ArrayAdapter
				.createFromResource(getActivity(), R.array.car_year_array,
						android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_two
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner_two.setAdapter(adapter_two);
		spinner_two.setOnItemSelectedListener(this);

		// Get the click events
		goButton = (Button) rootView.findViewById(R.id.button_go);
		goButton.setOnClickListener(this);
		selectButton = (Button) rootView.findViewById(R.id.button_select);
		selectButton.setOnClickListener(this);
		suggestionButton=(Button) rootView.findViewById(R.id.suggestion_button);
		suggestionButton.setOnClickListener(this);
		return rootView;
	}

	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// An item was selected. You can retrieve the selected item using
		// parent.getItemAtPosition(pos)
	}

	public void onNothingSelected(AdapterView<?> parent) {
		// Another interface callback
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_go:
			buyLayout.setVisibility(View.GONE);
			selectLayout.setVisibility(View.VISIBLE);
			break;
		case R.id.button_select:
			Fragment saveTargetFragment = new SavingTargetFragment();
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, saveTargetFragment).commit();
			break;			
		case R.id.suggestion_button:
			Fragment suggestionFragment = new SuggestedGoalFragment();
			FragmentManager fm= getFragmentManager();
			fm.beginTransaction()
					.replace(R.id.content_frame, suggestionFragment).commit();
			break;

		// TODO Auto-generated method stub

		}

	}
}