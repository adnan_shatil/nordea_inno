package com.example.android.innovation.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.example.android.innovation.R;

public class SavingTargetFragment extends Fragment implements
		OnSeekBarChangeListener,OnClickListener {

	SeekBar moneyBar, timeBar;
	TextView savingTarget, savingTime;
	TextView initialGoal;
	int totalCost=4000,totaTime=100;
	Button ok;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.saving_target, container,
				false);
		String title = getResources().getString(R.string.saving_target);
		getActivity().setTitle(title);

		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		moneyBar = (SeekBar) view.findViewById(R.id.euro_seekbar);
		timeBar = (SeekBar) view.findViewById(R.id.time_seekbar);
		savingTarget = (TextView) view.findViewById(R.id.saving_value);
		savingTime = (TextView) view.findViewById(R.id.month_value);
		initialGoal=(TextView) view.findViewById(R.id.target_amount);
		ok=(Button) view.findViewById(R.id.ok_button);
		ok.setOnClickListener(this);
		initialGoal.setText("40000 Euro");
		moneyBar.setMax(totalCost);
		timeBar.setMax(totaTime);

		moneyBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub

				// t1.setTextSize(progress);
				try{
					int timevalue=40000/progress;
					timeBar.setProgress(timevalue);
					savingTarget.setText(""+progress+ " "+"euro");
				}catch(Exception e){
					
				}
				

			}
		});

		timeBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				try{
					int moneyvalue=40000/progress;
					moneyBar.setProgress(moneyvalue);
					savingTime.setText(""+progress+ " "+"month");
				}catch(Exception e){
					
				}
				

			}
		});

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId()){
		case R.id.ok_button:
			Fragment spendOptFragment = new SpendingOptimizationFragment();
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, spendOptFragment).commit();
			break;
		}
		
	}
}
