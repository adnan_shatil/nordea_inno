package com.example.android.innovation.fragment;

import org.apache.http.Header;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.example.android.innovation.R;
import com.example.android.innovation.activity.MapActivity;
import com.example.android.innovation.adapter.ImageAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class FinancialProfileFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.profile, container, false);
		String title = getResources().getString(R.string.finan_pro);
		getActivity().setTitle(title);

		GridView gridview = (GridView) rootView.findViewById(R.id.gridview);
		gridview.setAdapter(new ImageAdapter(getActivity()));
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Fragment graphFragment = new GraphViewLayout(position);
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, graphFragment).commit();

			}
		});

		return rootView;
	}

	private void makeHttpCall() {
		AsyncHttpClient client = new AsyncHttpClient();
		client.get("http://www.google.com", new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"
				System.out.println("Succeess");
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)
				System.out.println("faila");
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried
			}
		});
	}

}
