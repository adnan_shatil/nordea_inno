package com.example.android.innovation.fragment;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.BubbleChartData;
import lecho.lib.hellocharts.model.BubbleValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.ColumnValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.util.Utils;
import lecho.lib.hellocharts.view.BubbleChartView;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.innovation.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class GraphViewLayout extends Fragment{

	private String[] graphTitles;
	private int position;
	private BubbleChartView bubblechart;
	private LineChartView linechart;
	private ColumnChartView uppercolumnchart;
	private ColumnChartView lowercolumnchart;
	private LineChartData upperlineData;
	private ImageView heatMap;
	

	public GraphViewLayout(int position) {
		this.position=position;
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.graph_view_layout, container,
				false);
		graphTitles = getResources().getStringArray(R.array.chart_title_array);
		getActivity().setTitle(graphTitles[position]);
		
		uppercolumnchart = (ColumnChartView)rootView.findViewById(R.id.uppercolumnchart);
		lowercolumnchart = (ColumnChartView)rootView.findViewById(R.id.lowercolumnchart);
		bubblechart = (BubbleChartView)rootView.findViewById(R.id.bubblechart);
		linechart = (LineChartView)rootView.findViewById(R.id.linechart);
		heatMap=(ImageView) rootView.findViewById(R.id.heat_map);
		
		setDataPoints();
		
//		columnchart.setVisibility(View.GONE);
//		bubblechart.setVisibility(View.GONE);
//		linechart.setVisibility(View.GONE);
		
		return rootView;
	}

	public void setDataPoints() {
		// Depending on postion create data
		// Create graph
		// show graoh
		switch(this.position) {
		case 0:
			//target vs actual spending line graph (similar to asking runrate)
			createTargetVsActualSpendingLineGraph();
			break;
		case 1:
			createMonthlyCategorySpendingColumnGraph();
			break;
		case 2:
			heatMap.setVisibility(View.VISIBLE);
			break;
		case 3:
			// shop expense bubble graph
			createShopExpenseBubbleGraph();
			break;
		default:
			break;
		}
	}
	
	void createTargetVsActualSpendingLineGraph() {
		linechart.setOnValueTouchListener(new ValueTouchListener());	// shows teh data value when touched, check the inner class
		
		//add values to teh graph
		final List<PointValue> values = new ArrayList<PointValue>();
		final List<PointValue> values2 = new ArrayList<PointValue>();
		
		final String[] months = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
				"Sep", "Oct", "Nov", "Dec", };
			final int numColumns = months.length;	// for 12 months
			final List<AxisValue> axisValues = new ArrayList<AxisValue>();

			AsyncHttpClient client = new AsyncHttpClient();
			client.get("http://ektara.eu-gb.mybluemix.net/ektara_api/year_target_actual", null, new JsonHttpResponseHandler() {
	            @Override
	            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
	                // If the response is JSONObject instead of expected JSONArray
	            }
	            
	            @Override
	            public void onSuccess(int statusCode, Header[] headers, JSONArray expenses) {
	                // pull out the elements of json
	                JSONObject oneExpense;
					try {
						for(int j=0; j<numColumns; j++) {
							oneExpense = (JSONObject)expenses.get(j);
							String month = oneExpense.getString("month");
							String expense = oneExpense.getString("expense");
							String target = oneExpense.getString("target");
			                // Do something with the response
							values.add(new PointValue(Float.parseFloat(month), Float.parseFloat(expense)));
							values2.add(new PointValue(Float.parseFloat(month), Float.parseFloat(target)));
												
							axisValues.add(new AxisValue(j, months[j].toCharArray()));
						}
						//adding values to make the lines
					    Line line = new Line(values).setColor(Color.BLUE).setCubic(true);
					    Line line2 = new Line(values2).setColor(Color.RED).setCubic(true);
					    List<Line> lines = new ArrayList<Line>();
					    lines.add(line);
					    lines.add(line2);
					    
					    // creating the data set of the graph, and adding the lines to it
					    LineChartData data = new LineChartData();
					    data.setLines(lines);
					    
					    // set the axis
						Axis axisX = new Axis(axisValues);
						Axis axisY = new Axis().setHasLines(true);
						data.setAxisXBottom(axisX);
						data.setAxisYLeft(axisY);
						
					    //setting the dataset to the chart UI
					    linechart.setLineChartData(data);
						linechart.setVisibility(View.VISIBLE);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        });
		
		}
	
	//monthly spending summary, and on click monthly categorywise spending
	void createMonthlyCategorySpendingColumnGraph() {
		final String[] months = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec", };
		final int numColumns = months.length;	// for 12 months
		final List<Column> columns = new ArrayList<Column>();
		final List<AxisValue> axisValues = new ArrayList<AxisValue>();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get("http://ektara.eu-gb.mybluemix.net/ektara_api/year_expense", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
            }
            
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray expenses) {
                // pull out the elements of json
                JSONObject oneExpense;
				try {
					for(int j=0; j<numColumns; j++) {
						oneExpense = (JSONObject)expenses.get(j);
						String month = oneExpense.getString("month");
						String amount = oneExpense.getString("expense");
		                // Do something with the response
						List<ColumnValue> values = new ArrayList<ColumnValue>();
						values.add(new ColumnValue(Float.parseFloat(amount), Color.BLUE));
						columns.add(new Column(values).setHasLabelsOnlyForSelected(true));					
						axisValues.add(new AxisValue(j, months[j].toCharArray()));
					}
					ColumnChartData data;
					data = new ColumnChartData(columns);
					
					data.setAxisXBottom(new Axis(axisValues).setHasLines(true));
					data.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(2));

					lowercolumnchart.setColumnChartData(data);
					
					lowercolumnchart.setVisibility(View.VISIBLE);
					
					lowercolumnchart.setOnValueTouchListener(new LowerValueTouchListener());
					
					generateUpperColumnChart(Color.GREEN);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });

	}
	
	
	// gets month index from the touch listener and then 
	// retries categorywise data from backend
	// and displays it
	void generateUpperColumnChart(int color) {
		final int numColumns = 10;	// for 10 catogories
		final int numSubColumns = 2;
		final List<Column> columns = new ArrayList<Column>();
		final List<AxisValue> axisValues = new ArrayList<AxisValue>();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get("http://ektara.eu-gb.mybluemix.net/ektara_api/month_detail_expense", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
            }
            
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray expenses) {
                // pull out the elements of json
                JSONObject oneExpense;
				try {
					List<ColumnValue> values;
					for(int j=0; j<numColumns; j++) {
						oneExpense = (JSONObject)expenses.get(j);
						String planned_exp = oneExpense.getString("expense_plan");
						String orig_exp = oneExpense.getString("expense_orig");
						String category = oneExpense.getString("category");
		                // Do something with the response
						values = new ArrayList<ColumnValue>();
						int color = Utils.pickColor();
						values.add(new ColumnValue(Float.parseFloat(planned_exp), color));
						values.add(new ColumnValue(Float.parseFloat(orig_exp), color));
						//axisValues.add(new AxisValue(j, months[j].toCharArray()));
						columns.add(new Column(values).setHasLabelsOnlyForSelected(true));
					}
										
					ColumnChartData data;
					data = new ColumnChartData(columns);
					Axis axisX = new Axis(axisValues);
					axisX.setHasLines(true);
					axisX.setName("Category");
					data.setAxisXBottom(new Axis(axisValues).setHasLines(true));
					data.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(2));

					uppercolumnchart.setColumnChartData(data);
					
					uppercolumnchart.setVisibility(View.VISIBLE);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
	}
		
	void createShopExpenseBubbleGraph() {
		final List<BubbleValue> values = new ArrayList<BubbleValue>();
		ValueShape shape = ValueShape.CIRCLE;
		
		bubblechart.setOnValueTouchListener(new BubbleValueTouchListener());	// shows teh data value when touched, check the inner class
		
		AsyncHttpClient client = new AsyncHttpClient();
		client.get("http://ektara.eu-gb.mybluemix.net/ektara_api/bubble_graph", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
            }
            
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray expenses) {
                // pull out the elements of json
                JSONObject oneExpense;
				try {
					for(int j=0; j<6; j++) {
						oneExpense = (JSONObject)expenses.get(j);
						String place = oneExpense.getString("place");
						String amount = oneExpense.getString("amount");
						//System.out.println("#"+place+"#"+ "  #"+amount+"#");
		                // Do something with the response
						BubbleValue value = new BubbleValue(1*(j+1), ((float) Math.random() * 100f), Float.parseFloat(amount)*200); // x, y are the axis values, z is the bubble size
						//value.setShape(shape);
						value.setColor(Utils.pickColor());
						value.setLabel(place.toCharArray());
						values.add(value);
					}
					
				    // creating the data set of the graph, and adding the lines to it
				    BubbleChartData data = new BubbleChartData(values);
				    data.setHasLabels(true);
				    
				    // set the axis
					Axis axisX = new Axis();
					Axis axisY = new Axis().setHasLines(true);
					axisX.setName("Axis X");
					axisY.setName("Axis Y");
					data.setAxisXBottom(axisX);
					data.setAxisYLeft(axisY);
					
				    //setting the dataset to the chart UI
				    bubblechart.setBubbleChartData(data);
				    
				    bubblechart.setVisibility(View.VISIBLE);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
		
		// make bubbles for the graph
		
//		BubbleValue value = new BubbleValue(1, 10, 1000); // x, y are the axis values, z is the bubble size
//		value.setShape(shape);
//		value.setColor(Utils.pickColor());
//		values.add(value);
			    

	}
		
	private class ValueTouchListener implements
			LineChartView.LineChartOnValueTouchListener {
		@Override
		public void onValueTouched(int selectedLine, int selectedValue,
				PointValue value) {
			Toast.makeText(getActivity().getBaseContext(), "Selected: " + value,
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onNothingTouched() {
			// TODO Auto-generated method stub
		}
	}
	

	private class LowerValueTouchListener implements
			ColumnChartView.ColumnChartOnValueTouchListener {
		@Override
		public void onValueTouched(int selectedLine, int selectedValue,
				ColumnValue value) {
			// TODO find the month and send it.
			generateUpperColumnChart(value.getColor());
		}

		@Override
		public void onNothingTouched() {
			generateUpperColumnChart(Utils.COLOR_GREEN);
		}
	}
	
	private class BubbleValueTouchListener implements
			BubbleChartView.BubbleChartOnValueTouchListener {
		@Override
		public void onValueTouched(int selectedBubble, BubbleValue value) {
			Toast.makeText(getActivity().getBaseContext(), "Selected: " + value,
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onNothingTouched() {
			// TODO Auto-generated method stub
		}
	}
	
	

}
