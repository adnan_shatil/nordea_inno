package com.example.android.innovation.fragment;

import com.example.android.innovation.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SpendingOptimizationFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.spending_optimization, container,
				false);
		String title = getResources().getString(R.string.saving_optimization);
		getActivity().setTitle(title);

		return rootView;
	}
}
