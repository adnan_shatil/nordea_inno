package com.ektara.graphandresttest;

import java.util.ArrayList;
import java.util.List;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.BubbleChartData;
import lecho.lib.hellocharts.model.BubbleValue;
import lecho.lib.hellocharts.model.ChartData;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.ColumnValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.Utils;
import lecho.lib.hellocharts.view.BubbleChartView;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import org.json.*;
import org.apache.http.*;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		createLineChart();
		
		createBubbleChart();
		
		createBarChart();
		
		testGetPost();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	void createLineChart() {
		LineChartView linechart = (LineChartView)findViewById(R.id.chart);
		linechart.setOnValueTouchListener(new ValueTouchListener());	// shows teh data value when touched, check the inner class
		
		// points for the first line of the graph
		List<PointValue> values = new ArrayList<PointValue>();
		values.add(new PointValue(0, 0));
		values.add(new PointValue(1, 1));
		values.add(new PointValue(1, 2));
		values.add(new PointValue(2, 2));
		
		// points for the second line fo the graph
		List<PointValue> values2 = new ArrayList<PointValue>();
		values2.add(new PointValue(2, 0));
		values2.add(new PointValue(1, 1));
		values2.add(new PointValue(0, 2));
		
		//adding values to make the lines
	    Line line = new Line(values).setColor(Color.BLUE).setCubic(true);
	    Line line2 = new Line(values2).setColor(Color.RED).setCubic(true);
	    List<Line> lines = new ArrayList<Line>();
	    lines.add(line);
	    lines.add(line2);
	    
	    // creating the data set of the graph, and adding the lines to it
	    LineChartData data = new LineChartData();
	    data.setLines(lines);
	    
	    // set the axis
		Axis axisX = new Axis();
		Axis axisY = new Axis().setHasLines(true);
		axisX.setName("Axis X");
		axisY.setName("Axis Y");
		data.setAxisXBottom(axisX);
		data.setAxisYLeft(axisY);
		
	    //setting the dataset to the chart UI
	    linechart.setLineChartData(data);
	}
	
	void createBubbleChart() {
		ValueShape shape = ValueShape.CIRCLE;
		
		BubbleChartView bubblechart = (BubbleChartView)findViewById(R.id.bubblechart);
		bubblechart.setOnValueTouchListener(new BubbleValueTouchListener());	// shows teh data value when touched, check the inner class
		
		// make bubbles for the graph
		List<BubbleValue> values = new ArrayList<BubbleValue>();
		BubbleValue value = new BubbleValue(1, 10, 1000); // x, y are the axis values, z is the bubble size
		value.setShape(shape);
		value.setColor(Utils.pickColor());
		values.add(value);
		BubbleValue value1 = new BubbleValue(2, 20, 500);
		value1.setShape(shape);
		value1.setColor(Utils.pickColor());
		values.add(value1);
		BubbleValue value2 = new BubbleValue(3, 30, 3000);
		value2.setShape(shape);
		value2.setColor(Utils.pickColor());
		values.add(value2);
		BubbleValue value3 = new BubbleValue(4, 40, 40000);
		value3.setShape(shape);
		value3.setColor(Utils.pickColor());
		values.add(value3);
			    
	    // creating the data set of the graph, and adding the lines to it
	    BubbleChartData data = new BubbleChartData(values);
	    //data.setHasLabels(true);
	    
	    
	    // set the axis
		Axis axisX = new Axis();
		Axis axisY = new Axis().setHasLines(true);
		axisX.setName("Axis X");
		axisY.setName("Axis Y");
		data.setAxisXBottom(axisX);
		data.setAxisYLeft(axisY);
		
	    //setting the dataset to the chart UI
	    bubblechart.setBubbleChartData(data);
	}
	
	void createBarChart() {
		ColumnChartView chart = (ColumnChartView)findViewById(R.id.columnchart);
		ColumnChartData data;
		int numSubcolumns = 1;
		int numColumns = 8;
		// Column can have many subcolumns, here by default I use 1 subcolumn in
		// each of 8 columns.
		List<Column> columns = new ArrayList<Column>();
		List<ColumnValue> values;
		for (int i = 0; i < numColumns; ++i) {
			values = new ArrayList<ColumnValue>();
			for (int j = 0; j < numSubcolumns; ++j) {
				values.add(new ColumnValue((float) Math.random() * 50f + 5,
						Utils.pickColor()));
			}
			Column column = new Column(values);
			column.setHasLabels(true);
			columns.add(column);
		}
		data = new ColumnChartData(columns);
		
		Axis axisX = new Axis();
		Axis axisY = new Axis().setHasLines(true);
		axisX.setName("Axis X");
		axisY.setName("Axis Y");
		data.setAxisXBottom(axisX);
		data.setAxisYLeft(axisY);

		chart.setColumnChartData(data);
	}
	
	private class ValueTouchListener implements
			LineChartView.LineChartOnValueTouchListener {
		@Override
		public void onValueTouched(int selectedLine, int selectedValue,
				PointValue value) {
			Toast.makeText(getBaseContext(), "Selected: " + value,
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onNothingTouched() {
			// TODO Auto-generated method stub
		}
	}
	
	private class BubbleValueTouchListener implements
			BubbleChartView.BubbleChartOnValueTouchListener {
		@Override
		public void onValueTouched(int selectedBubble, BubbleValue value) {
			Toast.makeText(getBaseContext(), "Selected: " + value,
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onNothingTouched() {
			// TODO Auto-generated method stub
		}
	}
	
	void testGetPost() {
		String tweetText = null;
		EktaraRestClient.get("statuses/public_timeline.json", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
            }
            
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline
                JSONObject firstEvent;
				try {
					firstEvent = (JSONObject)timeline.get(0);
					String tweetText = firstEvent.getString("text");
	                // Do something with the response
					TextView txtView = (TextView)findViewById(R.id.hellotext);
	                txtView.setText(tweetText);
	                
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
	}
}
