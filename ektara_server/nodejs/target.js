var pg = require('pg');
var lineReader = require('line-reader');
var shop_obj = require('./shop_obj');
var conString = "postgres://cgiuser:cgiuser@localhost/ektara";

exports.get = function(req, res){
 //    console.log(req.params.person_id);
 //    var person_id = req.params.person_id;
	// var client = new pg.Client(conString);
	// client.connect(function(err) {
	// 	if(err) {
	// 		return console.error('could not connect to postgres', err);
	// 	}
		
	// 	client.query('SELECT * from target where target_id=$1', [person_id], function(err, result) {
	// 		if(err) {
	// 			return console.error('error running query', err);
	// 		}
			
	// 		client.end();
	// 		res.send(result.rows);
	// 	});
	// });


	
	// /* csv file reader example */
	// lineReader.eachLine('Transactions_FI3111123500536740_20140101_20141231.csv', function(line, last) {
	// 	console.log(line);

	// 	var dt = line.split('\t');
	// 	// console.log(dt);

	// 	insertTransaction(dt[0], dt[1], dt[2], dt[3]);

	// 	// if (/* done */) {
	// 	// return false; // stop reading
	// 	// }
	// });



	getHistory(10001, res);

	// res.send({months: calcMonthlyTarget('2015-12-01')	});

};


function getHistory(account_id, res) {
	
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('select EXTRACT(MONTH FROM transaction_time) as month, EXTRACT(YEAR FROM transaction_time) as year ' +
				'from transaction ' +
				'where account_id = $1 ' +
				'group by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time) ' +
				'order by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time)', 
			[account_id],
			function(err, result) {
		
			// res.send(result.rows);
			getHistoryAgain(account_id, res);

			if(err) {
				return console.error('error running query', err);
			}
		
			client.end();

		});
	});
}

function getHistoryAgain(account_id, res) {
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('select (select sum(amount) ' +
			'from transaction ' + 
			'where amount < 0 ' +
			'and EXTRACT(MONTH FROM transaction_time) = $1 ' +
			'and EXTRACT(YEAR FROM transaction_time) = $2 ' +
			'and account_id = $3 ' +
			'group by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time) ' +
			') as expense, ' +
			'(select sum(amount) ' +
			'from transaction ' + 
			'where amount > 0 ' +
			'and EXTRACT(MONTH FROM transaction_time) = $1 ' +
			'and EXTRACT(YEAR FROM transaction_time) = $2 ' +
			'and account_id = $3 ' +
			'group by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time) ' +
			') as income ', 
			[9, 2014, account_id],
			function(err, result) {
		
				var inc = result.rows[0].income;
				var exp = result.rows[0].expense;
				var sav;
				if(inc)
					sav = inc + exp;
				else
					sav = 0;

				res.send({income: inc, expense: exp, saving: sav});

			if(err) {
				return console.error('error running query', err);
			}
						
			client.end();

		});
	});
}



function baalSaal(res) {
	var account_id = 10001;
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('select EXTRACT(MONTH FROM transaction_time) as month, EXTRACT(YEAR FROM transaction_time) as year ' +
				'from transaction ' +
				'where account_id = $1 ' +
				'group by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time) ' +
				'order by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time)', 
			[account_id],
			function(err, result) {			
				
				buildBalance(result.rows, res);
				client.end();
		});
	});	
}

function buildBalance(account_id, transaction_month_year, res) {
	// var account_id = 10001;
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('select EXTRACT(MONTH FROM transaction_time) as month, EXTRACT(YEAR FROM transaction_time) as year ' +
				'from transaction ' +
				'where account_id = $1 ' +
				'group by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time) ' +
				'order by EXTRACT(MONTH FROM transaction_time), EXTRACT(YEAR FROM transaction_time)', 
			[account_id],
			function(err, result) {			
				
				res.send(result.rows);
				client.end();
		});
	});
	
}




exports.set = function(req, res){
    console.log(req.body.person_id);

    var target_id = req.body.target_id;
    var person_id = req.body.person_id;
    var target_amount = req.body.target_amount; 
    var target_time = req.body.target_time; 
	var target_obj = req.body.target_obj;

	var monthly_target = calcMonthlyTarget(target_time, target_amount);

	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		var qry;
		var parm_arr = [];
		if(target_id) {
			qry = 'insert into target(person_id, target_amount, target_time, monthly_target, target_obj) ' + 
				'values($1, $2, to_timestamp($3, \'yyyy-dd-mm\'), $4, $5) returning target_id';
			parm_arr = [person_id, target_amount, target_time, target_obj, monthly_target];
		} else {
			qry = 'update target set target_amount=$1, target_time=to_timestamp($2, \'yyyy-dd-mm\'), ' +
				'monthly_target=$3, target_obj=$4 where target_id = $5 returning target_id';
			parm_arr = [target_amount, target_time, monthly_target, target_obj, target_id];
		}

		client.query(qry, parm_arr, function(err, result) {
			if(err) {
				return console.error('error running query', err);
			}
			
			console.log(result.rows[0].theTime);
			//output: Tue Jan 15 2013 19:12:47 GMT-600 (CST)
			client.end();

			res.send({ target_id: result.rows[0].target_id});
		});
	});

};

exports.calc = function(req, res) {
	// javascript split by tabconsole.log(req.body);	
}

function calcMonthlyTarget(target_time) {
	var monthly_target;

	var today = new Date();
	var target_time = new Date(target_time);

	var months;
    months = (target_time.getFullYear() - today.getFullYear()) * 12;
    months -= target_time.getMonth() + 1;
    months += today.getMonth();
    return months <= 0 ? 0 : months;


	// return target_time;
	// return monthly_target;
}

// function pg_sync(person_id) {
// 	var client = new pgsync.Client();
// 	client.connect("host=127.0.0.1 port=5432 dbname=ektara");
// 	client.begin();
// 	client.setIsolationLevelSerializable();
// 	var result = client.query('SELECT * from target where target_id=$1', [person_id]);
// 	client.disconnect();
// 	return result;
// }	

function insertTransaction(account_id, transaction_time, place, amount) {
	
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('INSERT INTO transaction (account_id,transaction_time,place,amount) ' + 
			'VALUES ($1, to_timestamp($2, \'yyyy-mm-dd\'), $3, $4) RETURNING transaction_id', 
			[account_id, transaction_time, place, amount],
			function(err, result) {
		
			t_id = result.rows[0].transaction_id;

			var data = shop_obj.search(place);

			updateTransaction(t_id, data.place_shortname, data.category);

			if(err) {
				return console.error('error running query', err);
			}
		
			client.end();

		});
	});
}

function updateTransaction(transaction_id, place_shortname, category) {
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('UPDATE transaction set place_shortname = $1,category = $2 where transaction_id = $3', 
			[place_shortname, category, transaction_id],
			function(err, result) {
		
			if(err) {
				return console.error('error running query', err);
			}
						
			client.end();

		});
	});
}