var pg = require('pg');
var conString = "postgres://ewrratan:DNGri8mK9z9O3nBgnH6_3Q9yD8x1uzmT@qdjjtnkv.db.elephantsql.com:5432/ewrratan";

exports.bubble_graph = function(req, res){
	
	var qry = 'select (sum(amount)*-1) as expense, category ' +
		'from transaction_backup ' +
		'where amount < 0 ' +
		'and EXTRACT(MONTH FROM transaction_time) = 7 ' +
		'and EXTRACT(YEAR FROM transaction_time) = 2014 ' +
		'group by category ' + 
		'order by expense desc ' +
		'limit 7';


	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query(qry, 
			[],
			function(err, result) {
		
			// res.send(result.rows);
			
			if(err) {
				return console.error('error running query', err);
			}
		
			res.send(result.rows);

			client.end();

		});
	});	
}

exports.year_expense = function(req, res){
		
	var qry = 'select month, (expense*-1) as expense ' +
		'from monthly_balance ' +
		'order by month';

	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query(qry, 
			[],
			function(err, result) {
		
			// res.send(result.rows);
			
			if(err) {
				return console.error('error running query', err);
			}
		
			res.send(result.rows);

			client.end();

		});
	});	
	
}

exports.month_detail_expense = function(req, res){
	
	var month = req.query.month;
	var year = req.query.year;

	// console.log(req);
	// console.log(year);

	var qry = 'select (sum(amount)*-1) as expense_orig, category ' + 
		'from transaction_backup ' + 
		'where amount < 0 ' +
		'and EXTRACT(MONTH FROM transaction_time) = $1 ' +
		'and EXTRACT(YEAR FROM transaction_time) = $2 ' +
		'group by category ' + 
		'order by expense_orig desc';

	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query(qry, 
			[month, year],
			function(err, result) {
		
			// res.send(result.rows);
			
			if(err) {
				return console.error('error running query', err);
			}
		
			res.send(result.rows);

			client.end();

		});
	});		
	
}

exports.year_target_actual = function(req, res){
	
	
	var qry = 'select month, (expense*-1) as expense ' +
		'from monthly_balance ' +
		'order by month';

	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query(qry, 
			[],
			function(err, result) {
		
			// res.send(result.rows);
			
			if(err) {
				return console.error('error running query', err);
			}
		
			res.send(result.rows);

			client.end();

		});
	});		
	
}





















// exports.bubble_graph = function(req, res){



// 	// var qry = 'select place_shortname as place, (sum(amount)*-1) as amount ' +
// 	// 		'from transaction_backup ' +
// 	// 		'where amount < 0 ' +
// 	// 		'and place_shortname not in ' +
// 	// 		'(\'other\', \'studenthousing\', \'school\') ' +
// 	// 		'group by place_shortname ' +
// 	// 		'order by sum(amount)';

// 	// var qry = 'select month, (expense*-1) as expense ' +
// 	// 			'from monthly_balance ' +
// 	// 			'order by month';

// 	var qry = 'select (sum(amount)*-1) as expense, category ' +
// 		'from transaction_backup ' +
// 		'where amount < 0 ' +
// 		'and EXTRACT(MONTH FROM transaction_time) = 7 ' +
// 		'and EXTRACT(YEAR FROM transaction_time) = 2014 ' +
// 		'group by category ' + 
// 		'order by expense desc';


// 	var client = new pg.Client(conString);
// 	client.connect(function(err) {
// 		if(err) {
// 			return console.error('could not connect to postgres', err);
// 		}
		
// 		client.query(qry, 
// 			[],
// 			function(err, result) {
		
// 			// res.send(result.rows);
			
// 			if(err) {
// 				return console.error('error running query', err);
// 			}
		
// 			res.send(result.rows);

// 			client.end();

// 		});
// 	});
// }


// /* monthly category expense */
// select category, (sum(amount)*-1) as amount
// from transaction_backup 
// where amount < 0 
// and category in 
// ('grocery', 'transport', 'health', 'cloth', 'food', 'electronics', 'entertainment')
// and EXTRACT(MONTH FROM transaction_time) = 1
// group by category
// order by sum(amount);

