var posts = [
	{ title: 'Foo', body: 'some foo bar' },
	{ title: 'Foo bar', body: 'more foo bar' },
	{ title: 'Foo bar baz', body: 'more foo bar baz' }
];

var pg = require('pg');
var shop_obj = require('./shop_obj');
var conString = "postgres://cgiuser:cgiuser@localhost/ektara";
var t_id;

exports.list = function(req, res){
    console.log(req.body.name);

    var conString = "postgres://cgiuser:cgiuser@localhost/ektara";

	var client = new pg.Client(conString);
	client.connect(function(err) {
	  if(err) {
	    return console.error('could not connect to postgres', err);
	  }
	  client.query('SELECT NOW() AS "theTime"', function(err, result) {
	    if(err) {
	      return console.error('error running query', err);
	    }
	    console.log(result.rows[0].theTime);
	    //output: Tue Jan 15 2013 19:12:47 GMT-600 (CST)
	    client.end();
	  });
	});

	res.send({ title: 'Posts', posts: posts });
};


exports.insert = function(req, res){
    
    // var temp = shop_obj.search('SUOMENLINNA');

	// var client = new pg.Client(conString);
	// client.connect(function(err) {
	// 	if(err) {
	// 		return console.error('could not connect to postgres', err);
	// 	}
		
	// 	client.query('SELECT NOW() AS "theTime"', function(err, result) {
		
	// 		if(err) {
	// 			return console.error('error running query', err);
	// 		}
						
	// 		client.end();

	// 		res.send({ time: result.rows[0].theTime });
	// 	});
	// });
	var account_id = 10001;
	var transaction_time = '2014-01-08';
	var place = 'Dabin Tmi';
	var amount = -6.55;

	insertTransaction(account_id, transaction_time, place, amount);


	// res.send({transaction_id: t_id});
};

function insertTransaction(account_id, transaction_time, place, amount) {
	
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('INSERT INTO transaction (account_id,transaction_time,place,amount) ' + 
			'VALUES ($1, to_timestamp($2, \'yyyy-dd-mm\'), $3, $4) RETURNING transaction_id', 
			[account_id, transaction_time, place, amount],
			function(err, result) {
		
			t_id = result.rows[0].transaction_id;

			var data = shop_obj.search(place);

			updateTransaction(t_id, data.place_shortname, data.category);

			if(err) {
				return console.error('error running query', err);
			}
		
			client.end();

		});
	});
}

function updateTransaction(transaction_id, place_shortname, category) {
	var client = new pg.Client(conString);
	client.connect(function(err) {
		if(err) {
			return console.error('could not connect to postgres', err);
		}
		
		client.query('UPDATE transaction set place_shortname = $1,category = $2 where transaction_id = $3', 
			[place_shortname, category, transaction_id],
			function(err, result) {
		
			if(err) {
				return console.error('error running query', err);
			}
						
			client.end();

		});
	});
}






