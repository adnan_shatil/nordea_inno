var express = require('express')
var bodyParser = require('body-parser')

var app = express();
var site = require('./site');
var post = require('./post');
var user = require('./user');
var target = require('./target');
var ektara_api = require('./graph_api');
// var shop_obj = require('./shop_obj');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.listen(3000);


app.get('/site/:person_id', site.index);

app.post('/posts', post.list);

app.get('/posts', post.insert);


app.get('/targets/get/:person_id', target.get);
app.post('/targets/set', target.set);
app.post('/targets/calc', target.calc);


app.get('/ektara_api/bubble_graph', ektara_api.bubble_graph);
app.get('/ektara_api/year_expense', ektara_api.year_expense);
app.get('/ektara_api/month_detail_expense', ektara_api.month_detail_expense);
app.get('/ektara_api/year_target_actual', ektara_api.year_target_actual);





app.post('/', function (req, res) {
  res.send('Got a POST request');
})

app.put('/user', function (req, res) {
  res.send('Got a PUT request at /user');
})

app.delete('/user', function (req, res) {
  res.send('Got a DELETE request at /user');
})